<?php

/**
 * Upload
 * 
 * @package mtx-tools
 * @author Tarchini Maurizio
 * @copyright 2017
 * @version 1.3.1
 * @access public
 * @license MIT
 */
abstract class Upload
{
    public $error = array();
    public $opt;
    public $filename;
    protected $extension;
    protected $name;
    protected $file;
    protected $uploadAbort;
    protected $safeName;
    
        public function __construct($options = array())
        {
            $defaults = array(  'uploadDir' => '',
                                'banExtensions' => false,
                                'allowedExtensions' => false,
                                'name' => 'upload_name',
                                'maxSize' => false,
                                'safeName' => false,
                                'nameMode' => false //or normalize or random
                                );
                                
            $this->opt = array_merge((array)$defaults,(array)$options);
            $this->file = $_FILES[$this->opt['name']];
            
            $this->getExtension();
            
            if($this->opt['nameMode'])
            {
                $this->getSafeName();
            }
            
            $this->uploadAbort = FALSE;
            
            $this->uploadProcess();
            
        }
        
        protected abstract function onSuccess();
        
        protected abstract function onAbort();
        
        protected function customControls()
        {
            return;
        }
        
        protected function getExtension()
        {
            $part = explode('.',$this->file['name']);
            $this->extension = end($part);
            $this->extension = strtolower($this->extension);
        }
        
        protected function getSafeName()
        {
            if($this->opt['nameMode'] == 'normalize')
            {
                $this->filename = $this->normalizeString($this->file['name']);
            }
            
            if($this->opt['nameMode'] == 'random')
            {
                $this->filename = $this->randomName();
            }
            
        }
        
        protected function randomName()
        {
            return sha1(microtime()) . '.' . $this->extension;
        }
        
       	protected function normalizeString($string)
		{
			$replaceArr = array(
    		'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
    		'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
    		'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
    		'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
    		'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
    		'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
    		'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f');
    		
    		$strResult = strtr($string, $replaceArr);
			$strResult = str_ireplace("'", "", $strResult);
            $strResult = str_ireplace(" ","-",$strResult);
			$strResult = preg_replace('/[^A-Za-z0-9 ._-]/', "", $strResult);
			$strResult = trim($strResult);
			$strResult = preg_replace('/[ ]{2,}/', "-", $strResult);
			$strResult = strtolower($strResult);
            
			if($this->opt['safeName'])
            {
                return $this->safeName($strResult);
            }
            else
            {
                return $strResult;
            }
			
		}
        
        protected function safeName($strResult)
        {
            if(file_exists($this->opt['uploadDir'] . $strResult))
            {
                $par1 = explode('.', $strResult);
                $ext = end($par1);
                $par2 = substr($strResult,0,(strlen($ext)+1)*-1);
                $par2 .= '_1';
                $strResult = $par2 . '.' . $this->extension;
                return $this->safeName($strResult);
            }
            else
            {
                return $strResult;
            }
        }
        
        protected function banExtension()
        {
            if($this->opt['banExtensions'])
            {
                $ext = $this->opt['banExtensions'];
            
                if(in_array($this->extension, $ext))
                {
                    $this->uploadAbort = TRUE;
                    $this->error[] = 'Extension .' . $this->extension . ' not allowed';
                }
            }
        }
        
        protected function allowedExtension()
        {
            if($this->opt['allowedExtensions'])
            {
                $ext = $this->opt['allowedExtensions'];
            
                if(!in_array($this->extension, $ext))
                {
                    $this->uploadAbort = TRUE;
                    $this->error[] = 'Extension .' . $this->extension . ' not allowed';
                }
            }
        }
            
        
        protected function allowedSize()
        {
            if($this->opt['maxSize'])
            {
                $size = filesize($this->file['tmp_name']);
                if($size > $this->opt['maxSize'])
                {
                    $this->uploadAbort = TRUE;
                    $this->error[] = 'The file size exceeds the maximum allowable size (' . $this->opt['maxSize'] . ' bytes)';
                }
            } 
        }
        
        protected function isUploadedFile()
        {
            if(!is_uploaded_file($this->file['tmp_name']))
            {
                $this->uploadAbort = TRUE;
                $this->error[] = 'The file has been loaded improperly';
            }
        }
        
        protected function parseError()
        {
            switch($this->file['error'])
            {
                case UPLOAD_ERR_INI_SIZE:
                $this->error[] = 'The uploaded file exceeds the upload_max_filesize directive in php.ini (UPLOAD_ERR_INI_SIZE)';
                $this->uploadAbort = TRUE;
                break;

                case UPLOAD_ERR_PARTIAL:
                $this->error[] = 'The uploaded file was only partially uploaded (UPLOAD_ERR_PARTIAL)';
                $this->uploadAbort = TRUE;
                break;

                case UPLOAD_ERR_NO_FILE:
                $this->error[] = 'No file was uploaded (UPLOAD_ERR_NO_FILE)';
                $this->uploadAbort = TRUE;
                break;

                case UPLOAD_ERR_NO_TMP_DIR:
                $this->error[] = 'Missing a temporary folder (UPLOAD_ERR_NO_TMP_DIR)';
                $this->uploadAbort = TRUE;
                break;

                case UPLOAD_ERR_CANT_WRITE:
                $this->error[] = 'Failed to write file to disk (UPLOAD_ERR_CANT_WRITE)';
                $this->uploadAbort = TRUE;
                break;
                
                case UPLOAD_ERR_EXTENSION:
                $this->error[] = ' PHP extension stopped the file upload (UPLOAD_ERR_EXTENSION)';
                $this->uploadAbort = TRUE;
            }  
        }
        
        protected function uploadProcess()
        {
            $this->banExtension();
            $this->allowedExtension();
            $this->allowedSize();
            $this->isUploadedFile();
            $this->customControls();
            $this->parseError();
            
            if(!$this->uploadAbort)
            {
                if(@move_uploaded_file($this->file['tmp_name'], $this->opt['uploadDir'] . $this->filename))
                {
                    $this->error[] = 'File uploaded successfully!'; 
                }
                else
                {
                    $this->error[] = 'It was not possible to move the files from the temporary folder to the destination.';
                    $this->uploadAbort = TRUE;
                }
            }
            
            if($this->uploadAbort)
            {
                $this->onAbort();
            }
            else
            {
                $this->onSuccess();
            }

        }
}

?>