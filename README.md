UPLOAD - PHP
======

UPLOAD - PHP is a tool for the advanced management of file uploads via php.

##Installation and use

###1 Include upload.class.php

```bash
require_once 'path_to/upload.class.php';
```

###2 Extend upload class

upload is an abstract class with abstract methods. it is therefore necessary to extend and implement the abstract methods.

The methods to implement are onSuccess (executed when the success of the procedure) and OnAbort (performed in case of errors)

```bash
class Upload_file extends Upload
{
    protected function onAbort()
    {
        $string = "ERROR: There is one or more errors and the file was not loaded. Here is the error log:<br /><br />";
        foreach($this->error as $error)
        {
            $string .= '- ' . $error . "<br />";
        }
        echo $string;
    }
```  
Note: the array "error" contains the list of errors that have occurred during the upload process

```bash  
    protected function onSuccess()
    {
        echo 'uploaded file as: ' . $this->filename;
    }
```
Note: the "filename" variable contains the name by which the file is saved on the server

###3 Instantiates the upload_file class with arguments

Arguments:

- uploadDir: the directory where the file is saved (default '')
- banExtensions: an array of banned extensions (default false)
- allowedExtensions: an array of allowed extensions (default false)
- name: the value of "name" in the form file field (<input type="file" name="upload_file" /> default upload_file)
- maxSize: the maximum file size in bytes (default false (upload_max_filesize & post_max_size php.ini))
- safeName: (if true) if the file name already exists, _1 adds at the end of the name (default false > overwrite the existing file)
- nameMode: if 'random' save the file with a random name. if 'normalize' transforms the file name into a normalized string (no accents, spaces, symbols) like a wordpress permalink. if false no change to the file name (deprecated).

```bash
$opt = array('uploadDir' =>  '/mydir/',
            'allowedExtensions' => array('png','jpg'),
            'nameMode' => 'normalize',
            'safeName' => true);

$upload = new Upload_file($opt);
```

if you need more control, you can implement the optional method "customControls"

```bash    
    protected function customControls()
    {
        if($this->file['name'] = 'bad_file.txt')
        {
            $this->uploadAbort = true;
            $this->error[] = 'File name not allowed';
        }
    }
```
Note that if you want interrupts the upload process, you have to set the uploadAbort variable (true). Optionally you can enter the error in the array 'errors'.


