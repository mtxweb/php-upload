<?php
include dirname(__FILE__) . '/../lib/upload.class.php';

class Upload_file extends Upload
{
    protected function onAbort()
    {
        $string = "ERROR: There is one or more errors and the file was not loaded. Here is the error log:<br /><br />";
        foreach($this->error as $error)
        {
            $string .= '- ' . $error . "<br />";
        }
        echo $string;
    }
    
    protected function onSuccess()
    {
        echo 'uploaded file as: ' . $this->filename;
    }
    
    protected function customControls()
    {
        if($this->file['name'] = 'bad_file.txt')
        {
            $this->uploadAbort = true;
            $this->error[] = 'File name not allowed';
        }
    }

}

$opt = array('uploadDir' =>  '',
            'allowedExtensions' => false,
            'maxSize'=> false,
            'name' => 'upload_file',
            'nameMode' => 'normalize',
            'safeName' => true);

$upload = new Upload_file($opt);
?>